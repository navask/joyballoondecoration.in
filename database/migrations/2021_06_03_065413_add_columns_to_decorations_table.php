<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToDecorationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('decorations', function (Blueprint $table) {

            $table->string('title');
            $table->string('description');
            $table->string('image');
            $table->string('ac_price');
            $table->string('off_price');
            $table->boolean('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('decorations', function (Blueprint $table) {

            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('image');
            $table->dropColumn('ac_price');
            $table->dropColumn('off_price');
            $table->dropColumn('status')->default(1);
        });
    }
}
