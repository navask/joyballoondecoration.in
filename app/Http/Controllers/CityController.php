<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('cities.add_city');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'city' => 'required'
        ]);

        $city = $request->input('city');

        $data = array('name'=>$city);

        $res = City::insert($data);
        if($res){
            return back()->with('success','City Added Successfully');

        }
        else{
            return back()->with('failed','Something went wrong!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $cities = City::get();
       return view('cities.view_cities',compact('cities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = City::where('id',$id)->get();
        return view('cities.edit_city',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'city' => 'required',
        ]);

        $city = $request->input('city');
        $res = City::where('id',$id)->update(['name'=>$city]);
        if($res){
            return back()->with('success','City detailes updated..!');
        }
        else{
            return back()->with('failed','Something went wrong..!');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = City::where('id',$id)->delete();
        if($res){
            return back()->with('success','City Deleted..!');
        }
        else{
            return back()->with('danger','Something went wrong..!');
        }
    }
    public function status_change($id)
    {
        $status = City::where('id',$id)->get('status');
        if($status[0]['status'] == 1){
        City::where('id',$id)->update(['status'=>0]);
        return back()->with('success','Status updated to Active..!');
        }
        else{
        City::where('id',$id)->update(['status'=>0]);
        return back()->with('success','Status updated to Inctive..!');
        }
    }
}
