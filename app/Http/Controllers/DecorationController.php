<?php

namespace App\Http\Controllers;

use App\Models\Decoration;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isEmpty;

class DecorationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('decorations.add_decorations');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Title'=>'required',
            'Description'=>'required',
            'terms_and_conditions'=>'required',
            'Image'=>'required|mimes:jpg,jpeg,png',
            'Actual_Price'=>'required',
            'Offer_Price'=>'required|lt:Actual_Price',
        ]);

        $title = $request->get('Title');
        $description = $request->get('Description');
        $tandc = $request->get('terms_and_conditions');
        $ac_price = $request->get('Actual_Price');
        $off_price = $request->get('Offer_Price');
        $image = $request->get('Image');
        $id_title = str_replace(' ','-',strtolower($title));

        if($request->file('Image'))
        {
            $file = $request->file('Image');
            $filename = $request->file('Image')->getClientOriginalName();
            $filePath = 'decorationsimage/';
            $file->move($filePath, $filename);
        }

        $data = array([
            'title'=>$title,
            'description'=>$description,
            'image'=>$filename,
            'ac_price'=>$ac_price,
            'off_price'=>$off_price,
            'id_as_title' => $id_title,
            'tandc' => $tandc,
        ]);

        $res = Decoration::insert($data);

        if($res){
            return back()->with('success','Decoration added successfully..!');
        }
        else{
            return back()->with('failed','Something went wrong..!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Decoration  $decoration
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $decorations = Decoration::get();
        return view('decorations.view_decorations',compact('decorations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Decoration  $decoration
     * @return \Illuminate\Http\Response
     */
    public function status_change($id)
    {
        $status = Decoration::where('id',$id)->get('status');
        if($status[0]['status'] == 1){
        Decoration::where('id',$id)->update(['status'=>0]);
        return back()->with('success','Status updated to Inctive..!');
        }
        else{
        Decoration::where('id',$id)->update(['status'=>1]);
        return back()->with('success','Status updated to Active..!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Decoration  $decoration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'Title'=>'required',
            'Description'=>'required',
            'terms_and_conditions'=>'required',
            'Actual_Price'=>'required',
            'Offer_Price'=>'required',
        ]);

        $title = $request->get('Title');
        $description = $request->get('Description');
        $tandc = $request->get('terms_and_conditions');
        $ac_price = $request->get('Actual_Price');
        $off_price = $request->get('Offer_Price');
        $image = $request->file('Image');
        $id_title = str_replace(' ','-',strtolower($title));

            if ($request->file('Image')) {
                $file = $request->file('Image');
                $filename = $request->file('Image')->getClientOriginalName();
                $filePath = 'decorationsimage/';
                $file->move($filePath, $filename);

                $res = Decoration::where('id', $id)->update([
                    'title'=>$title,
                    'description'=>$description,
                    'image'=>$filename,
                    'ac_price'=>$ac_price,
                    'off_price'=>$off_price,
                    'id_as_title' => $id_title,
                    'tandc' => $tandc,
                    ]);

                if ($res) {
                    return back()->with('success', 'Decoration updated successfully..!');
                } else {
                    return back()->with('failed', 'Something went wrong..!');
                }
            }
            else{
                    $res = Decoration::where('id', $id)->update([
                    'title'=>$title,
                    'description'=>$description,
                    'ac_price'=>$ac_price,
                    'off_price'=>$off_price,
                    'id_as_title' => $id_title,
                    'tandc' => $tandc,
                        ]);

                    if ($res) {
                        return back()->with('success', 'Decoration updated successfully..!');
                    } else {
                        return back()->with('failed', 'Something went wrong..!');
                    }
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Decoration  $decoration
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Decoration::where('id',$id)->delete();
        if($res){
            return back()->with('success','Decoration Deleted..!');
        }
        else{
            return back()->with('danger','Something went wrong..!');
        }
    }

    public function edit($id)
    {
        $data = Decoration::where('id',$id)->get();
        return view('decorations.edit',compact('data'));
    }

    public function view_decoration($city,$title)
    {
        $decoration = Decoration::where('id_as_title',$title)->first();
        $id= $decoration['id'];

        $previous = Decoration::where('id', '<', $id)->max('id');
        $next = Decoration::where('id', '>', $id)->min('id');
        if(!empty($previous)){
            $previous_head = Decoration::where('id',$previous)->get('id_as_title');
            $previous_head = $previous_head[0]['id_as_title'];
        }
        else{
            $previous_head = Decoration::where('id',$id)->get('id_as_title');
            $previous_head = $previous_head[0]['id_as_title'];
        }
        if(!empty($next)){
            $next_head = Decoration::where('id',$next)->get('id_as_title');
            $next_head = $next_head[0]['id_as_title'];
        }
        else{
            $next_head = Decoration::where('id',$id)->get('id_as_title');
            $next_head = $next_head[0]['id_as_title'];
        }

            return view('pages.enquiry',compact('decoration','previous_head','next_head'));

    }
}
