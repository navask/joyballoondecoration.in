<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="../../index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>

    <!-- Right navbar links -->

  </nav>
  <!-- /.navbar -->

  @extends('assets.side_menus')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Decorations</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Admin</a></li>
              <li class="breadcrumb-item active">Edit decoration</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit decoration</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/update_decoration/{{$data[0]['id']}}" enctype="multipart/form-data">
              @csrf
                <div class="card-body">
                @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
                @endif
                @if (\Session::has('failed'))
                <div class="alert alert-danger">
                    <ul>
                        <li>{!! \Session::get('failed') !!}</li>
                    </ul>
                </div>
                @endif
                <div class="form-group">
                @if($errors->any())
                    {!! implode('', $errors->all('<div style="color: red;">:message</div>')) !!}
                @endif
                  <div class="form-group col-md-6">
                    <label for="city">Decoration Title</label>
                    <input type="text" value="{{$data[0]['title']}}" class="form-control" name="Title" id="city" placeholder="Enter Title for Decoration..">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="city">Decoration Descriptions</label>
                    <textarea style="height: 120px;" class="form-control" name="Description" id="description" placeholder="Enter descriptions for Decoration..">{{$data[0]['description']}}</textarea>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="city">Terms and conditions</label>
                    <textarea style="height: 120px;" class="form-control" name="terms_and_conditions" id="description" placeholder="Enter terms and conditions for this Decoration..">{{$data[0]['tandc']}}</textarea>
                  </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                  </div> -->
                  <div class="form-group col-md-6" style="margin-top: 25px;">

                <img id="blah" src="/decorationsimage/{{$data[0]['image']}}" style="height: 150px;width: 200px;"/>
                </div>
                  <div class="form-group col-md-3">
                    <div class="input-group">
                      <div class="custom-file">
                        <input name="Image" type="file" class="custom-file-input" id="imgInp">
                        <label class="custom-file-label" for="exampleInputFile">Change Image</label>
                      </div>
                      <div class="input-group-append">
                        <!-- <span class="input-group-text">Upload</span> -->
                      </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="form-group col-md-3">
                    <label for="city">Actual Price</label>
                    <input type="text" value="{{$data[0]['ac_price']}}" class="form-control" name="Actual_Price" id="city"  placeholder="Enter Actual Price..">
                  </div>
                  <div class="form-group col-md-3">
                    <label for="city">Offer Price</label>
                    <input type="text" class="form-control" value="{{$data[0]['off_price']}}" name="Offer_Price" id="city"  placeholder="Enter Offer Price..">
                  </div>
                  </div>

                  <!-- <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                  </div> -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer col-md-6">
                <center>
                <button type="submit" class="btn btn-primary">Update Decoration</button>

                </center>
                </div>
              </form>
            </div>


          </div>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @extends('assets.admin_footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="../../plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
$(function () {
  bsCustomFileInput.init();
});
</script>

<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
    readURL(this);
});
    </script>
</body>
</html>
