
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="The Best Place To Book Themed Birthday Decorations, First Birthday Party Decorations, Balloon Decoration For Birthdays At Home, Entertainment For Kids, Photographer And Videographer In Bangalore">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="/css/bootstrap4.css" type="text/css">
        <link rel="stylesheet" href="/css/style.css" type="text/css">
         <link rel="stylesheet" href="/css/style3.css" type="text/css">
          <link rel="stylesheet" href="/css/animate.css">

        <link rel="stylesheet" href="/css/jquery.mCustomScrollbar.min.css" type="text/css">
        <link rel="stylesheet" href="/css/media-queries.css" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="icon" href="/favicon/favicon-16x16.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/demo.css">
        <!-- Pushy CSS -->
        <link rel="stylesheet" href="/css/pushy.css">

          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <!--JQuery-->
<script type="text/javascript" src="/jquery-3.3.1.min.js"></script>
<!--Floating WhatsApp css-->
<link rel="stylesheet" href="/floating-wpp.min.css">
<!--Floating WhatsApp javascript-->
<script type="text/javascript" src="/floating-wpp.min.js"></script>

        <title>Themed Birthday Decorations At Home & Balloon Decoration For Birthdays In Bangalore | Themedbirthdays.in</title>
    </head>
    <body>



            <!-- Navigation -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light shadow" style="height:175px";>

                <div class="container">
                <div class="row">

             <div class="col-sm-12">

                    <a class="navbar-brand" href="#"><img src="/images/Themed Birthdays Logo 2.png"/ ></a>
                    <center> <p class="trending"style="font-weight:700;font-size:11px;color: red;margin-left:9%;">FOR TRENDING & CLASSIC BIRTHDAY DECORATIONS</p></center>
                      <div class="hello text-center">

                    <span class="material-icons" >call</span> +918105363088
                    <span class="material-icons">email</span> chintupartyandevents@gmail.com

                </div>
                  <!-- start nav Menu -->
        <nav class="pushy pushy-right" data-focus="#first-link">
            <div class="pushy-content">
                <ul>


                    <li class="pushy-link"><a href="home.html">HOME</a></li>
                    <li class="pushy-link"><a href="#section1">ABOUT US</a></li>
                    <li class="pushy-link"><a href="#section2">GALLERY</a></li>
                    <li class="pushy-link"><a href="#section3">CONTACT</a></li>
                </ul>
            </div>
        </nav>

        <!-- Site Overlay -->


        <!-- Your Content -->

            <!-- Menu Button -->
            <button class="menu-btn">&#9776; </button>




       <!-- close nav menu -->
                    </div>


                   </div>

                </div>
            </nav>

        <!-- Navigation -->

        <!-- Page Content -->
<div class="section-5-container section-container" id="section-5">
              <div class="container">


                      <div class="col section-5 section-description wow fadeIn">
                          <center> <u style="color:red;margin-bottom: 3%;" >
                          <h3  style="color:red;margin-bottom: 3%;font-size: 18pt;font-weight: 800;text-transform: uppercase;">
                          PACKAGE DETAILS</h3></u></center>

                  </div>
                  <div class="row" style="padding:15px;">
                      <div class="col-md-6 section-5-box wow fadeInUp">
                        <div class="section-5-box-image"><img src="/decorationsimage/{{$decoration['image']}}" alt="portfolio-1"width="100%"></div>
                        <center><h3 ><a href="#" style="color:red;">{{$decoration['title']}}</a></h3>
                            <div class="product-price">
                     <span class="red">₹{{$decoration['off_price']}}.00</span>
                     <span class="old">₹{{$decoration['ac_price']}}.00</span>
                     </div>

                        <a href="#" class="btn bg-red">Book Now</a></center>
                        </div>

                      <div class="col-md-6 section-5-box wow fadeInUp">


                    <!-- <h3 ><a href="#" style="color:black;font-size:15pt;"> BACKDROP DECORATIONS</a></h3>
                    <p>7/7 feet size Square shape curtain will be used as the backdrop. LED lights will be put behind the curtain. Paper flowers will be put on the backdrop.</p>
                    <h3 ><a href="#" style="color:black;font-size:15pt;"> ENTRANCE DECORATIONS</a></h3>
                    <p>Welcome board made of sunpack, mounted on a easel stand will be put up near the entrance. </p>
                    <h3 ><a href="#" style="color:black;font-size:15pt;"> HALL DECORATIONS</a></h3>
                    <p>120 nos metallic balloons in clusters of 5 will be put across the hall. 30 nos of loose balloons on floor.</p> -->

                    <h3 ><a href="#" style="color:black;font-size:15pt;"> INCLUSION</a></h3>
                    <p><?php print_r(str_replace('*',"<br><i style='color:red' class='fas fa-hand-point-right'></i>",$decoration['description']));?></p>

                    <br><br>
                    <h3 ><a href="#" style="color:black;font-size:15pt;"> TERMS AND CONDITIONS</a></h3>


                    <p><?php print_r(str_replace('*',"<br><i style='color:red' class='fas fa-hand-point-right'></i>",$decoration['tandc']));?></p>


                    <a href="@if(!empty($previous_head)){{ $previous_head}} @else <?php echo "#"; ?> @endif" class="btn bg-red"><i class="fa fa-arrow-left" aria-hidden="true"> Previous</i></a>
                    <a style="margin-left: 30%;" href="@if(!empty($previous_head)){{ $next_head}} @else <?php echo "#"; ?> @endif" class="btn bg-red">Next <i class="fa fa-arrow-right" aria-hidden="true"></i></a>

                  </div>

                  <div class="row col-md-12">

                <!-- <a href="@if(!empty($previous_head)){{ $previous_head}} @else <?php echo "#"; ?> @endif" class="btn bg-red"><i class="fa fa-arrow-left" aria-hidden="true"> Previous</i></a>
                <a style="margin-left: 50%;" href="@if(!empty($previous_head)){{ $next_head}} @else <?php echo "#"; ?> @endif" class="btn bg-red">Next <i class="fa fa-arrow-right" aria-hidden="true"></i></a> -->

                  </div>


              </div>
            </div>


        <!--activities Start-->

            <!--activities close-->




<div id="WAButton"></div>


<!--testimonial close-->

       <!--Footer Start-->
        <!-- Section 6 -->
        <section class="content-section" data-background="#009a4e" style="background: #fff;border-top: 3px solid red;box-shadow: 0 .7rem 5rem rgba(0,0,0,.15)!important;">
            <div class="col-12 spacing-100"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="accordion-box">
                            <h1 style="color:red;">FAQ'S</h1>

                            <div class="accordion" id="accordion" role="tablist">
                                <div class="row">

<div class="col-sm-5 section-6-box wow fadeInUp">
                                        <!-- end card -->
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo"> <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> What is Themed Birthday..?
                                                </a> </div>
                                            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body"> THEMED BIRTHDAYS are known to be a safe pair of hands. We provide BIRTHDAY DECORATION and BALLOON DECORATION IN BANGALORE and city outskirts.</div>
                                                <!-- end card-body -->
                                            </div>
                                            <!-- end collapse -->
                                        </div>

                                        <!-- end card -->
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo"> <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseTwo"> What is Themed Birthday..?
                                                </a> </div>
                                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body"> korfkdfkdk</div>
                                                <!-- end card-body -->
                                            </div>
                                            <!-- end collapse -->
                                        </div>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo"> <a class="collapsed" data-toggle="collapse" href="#collapse4" aria-expanded="false" aria-controls="collapseTwo"> What is Themed Birthday..?
                                                </a> </div>
                                            <div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body"> THEMED BIRTHDAYS are known to be a safe pair of hands. We provide BIRTHDAY DECORATION and BALLOON DECORATION IN BANGALORE and city outskirts.</div>
                                                <!-- end card-body -->
                                            </div>
                                            <!-- end collapse -->
                                        </div>
                                         <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo"> <a class="collapsed" data-toggle="collapse" href="#collapse5" aria-expanded="false" aria-controls="collapseTwo"> What is Themed Birthday..?
                                                </a> </div>
                                            <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body"> THEMED BIRTHDAYS are known to be a safe pair of hands. We provide BIRTHDAY DECORATION and BALLOON DECORATION IN BANGALORE and city outskirts.</div>
                                                <!-- end card-body -->
                                            </div>
                                            <!-- end collapse -->
                                        </div>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo"> <a class="collapsed" data-toggle="collapse" href="#collapse6" aria-expanded="false" aria-controls="collapseTwo"> What is Themed Birthday..?
                                                </a> </div>
                                            <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body"> THEMED BIRTHDAYS are known to be a safe pair of hands. We provide BIRTHDAY DECORATION and BALLOON DECORATION IN BANGALORE and city outskirts.</div>
                                                <!-- end card-body -->
                                            </div>
                                            <!-- end collapse -->
                                        </div>

  </div>
    <div class="col-sm-4 section-6-box wow fadeInUp">
        <div class="footer-first"><senter>
       <img src="/images/joy balloon.png" width="40%" >
        <div class="section-6-social">
                            <a href="#"><i class="fab fa-facebook-f"></i></a>

                  <a href="#"><i class="fab fa-twitter"></i></a>
                  <a href="#"><i class="fab fa-instagram"></i></a>

            </div>
         <a href="#" style="color:red;font-weight: 700;font-size: 14pt;">Site Map</a></center>
         <!--Div where the WhatsApp will be rendered-->


        </div>
     </div>
     <div class="col-sm-3 section-6-box wow fadeInUp">
    <div class="footer-last">
        <p style="font-weight: 700;font-size: 14px;color: red;"> <span class="material-icons" >call</span> +918105363088</p>
     <p style="font-weight: 700;font-size: 14px;color: red;"> <span class="material-icons">email</span> partyandevents@gmail.com </p>
         <p style="font-weight: 700;font-size: 14px;color: red;">

              Electronic City,<br>
             Bengaluru,<br>
             Karnataka, 560100
         </p>
     </div>
               </div>

                                </div>

                            </div>
                        </div>


                </div>
            </div></section>











            <!-- Footer -->

            <div class="copy text-center" style="color: white">
                 <div class="container">
                <div class="col-sm-8 section-6-box wow fadeInUp" style="background: red;">
                    <p>Copyright © Themed Birthday 2021 all rights reserved</p>
                </div>
                <div class="col-sm-4 section-6-box wow fadeInUp" style="background: red;">
                    <p>Designed by<a href="www.brandingsparrows.com"> Branding Sparrow</a></p>
                </div>
            </div></div>

    <!--Footer End-->
        <!--Footer End-->
<script type="text/javascript">
   $(function () {
           $('#WAButton').floatingWhatsApp({
               phone: 'WHATSAPP-PHONE-NUMBER', //WhatsApp Business phone number
               headerTitle: 'Chat with us on WhatsApp!', //Popup Title
               popupMessage: 'Hello, how can we help you?', //Popup Message
               showPopup: true, //Enables popup display
               buttonImage: '<img src="whatsapp.svg" />', //Button Image
               //headerColor: 'crimson', //Custom header color
               //backgroundColor: 'crimson', //Custom background button color
               position: "right" //Position: left | right

           });
       });
</script>



        <!-- Optional JavaScript -->
        <script src="/js/jquery-3.3.1.slim.min.js" type="text/javascript"></script>
        <script src="/js/popper.min.js" type="text/javascript"></script>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="/js/bootstrap.js" type="text/javascript"></script>
        <script src="/js/bootstrap.min.js" type="text/javascript"></script>


            <script src="/js/jquery-3.3.1.min.js"></script>
        <script src="/js/jquery.backstretch.min.js"></script>
        <script src="/js/wow.min.js"></script>
        <script src="/js/jquery.waypoints.min.js"></script>
        <script src="/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="/js/scripts.js"></script>

<script src="/js/pushy.min.js"></script>

    </body>
</html>
