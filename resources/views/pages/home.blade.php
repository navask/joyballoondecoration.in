
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="The Best Place To Book Themed Birthday Decorations, First Birthday Party Decorations, Balloon Decoration For Birthdays At Home, Entertainment For Kids, Photographer And Videographer In Bangalore">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap4.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
         <link rel="stylesheet" href="css/style3.css" type="text/css">
          <link rel="stylesheet" href="css/animate.css">

        <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css" type="text/css">
        <link rel="stylesheet" href="css/media-queries.css" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="icon" href="favicon/favicon-16x16.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/demo.css">
        <!-- Pushy CSS -->
        <link rel="stylesheet" href="css/pushy.css">

          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <!--JQuery-->
<script type="text/javascript" src="jquery-3.3.1.min.js"></script>
<!--Floating WhatsApp css-->
<link rel="stylesheet" href="floating-wpp.min.css">
<!--Floating WhatsApp javascript-->
<script type="text/javascript" src="floating-wpp.min.js"></script>

        <title>Themed Birthday Decorations At Home & Balloon Decoration For Birthdays In Bangalore | Themedbirthdays.in</title>
    </head>
    <body>



            <!-- Navigation -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light shadow" style="height:175px";>

                <div class="container">
                <div class="row">

             <div class="col-sm-12">

                    <a class="navbar-brand" href="#"><img src="images/Themed Birthdays Logo 2.png"/ ></a>
                    <center> <p class="trending"style="font-weight:700;font-size:11px;color: red;margin-left:9%;">FOR TRENDING & CLASSIC BIRTHDAY DECORATIONS</p></center>
                      <div class="hello text-center">

                    <span class="material-icons" >call</span> +918105363088
                    <span class="material-icons">email</span> chintupartyandevents@gmail.com

                </div>
                  <!-- start nav Menu -->
        <nav class="pushy pushy-right" data-focus="#first-link">
            <div class="pushy-content">
                <ul>


                    <li class="pushy-link"><a href="home.html">HOME</a></li>
                    <li class="pushy-link"><a href="#section1">ABOUT US</a></li>
                    <li class="pushy-link"><a href="#section2">GALLERY</a></li>
                    <li class="pushy-link"><a href="#section3">CONTACT</a></li>
                </ul>
            </div>
        </nav>

        <!-- Site Overlay -->


        <!-- Your Content -->

            <!-- Menu Button -->
            <button class="menu-btn">&#9776; </button>




       <!-- close nav menu -->
                    </div>


                   </div>

                </div>
            </nav>

        <!-- Navigation -->

<div class="container-fluid">
<center>
<div style="height: 50px;margin-top: -20px;font-size: 16.5px;"><b><label style="color: red;">BALLOON DECORATION NEAR {{strtoupper (str_replace('-',' ',Request::route ('city'))) }}</label> </b></div>
</center>
  <div id="hero">

                            <video autoplay loop muted playsinline preload="auto" style="width:100%;margin-top: -2%;">

                                <source src="video/video.mp4" type="video/mp4">


                            </video>

                </div>
</div>



<section id="section1">


        <!-- Page Content -->
       <div class="section-2-container section-container section-container-gray-bg" id="section-2" >
              <div class="container">

                  <div class="row">
                      <div class="col section-2 section-description wow fadeIn" style="">

                      </div>
                  </div>
                  <div class="row">

                    <div class="col-sm-12 section-2-box wow fadeInLeft">

             <center><h3>ABOUT US</h3>
                          <p class="medium-paragraph" style="font-size:14pt;">
                            If you are looking for<strong> THEMED BIRTHDAY PARTY </strong> PLANNERS AND DECORATORS IN BANGALORE then you have landed in the right page. THEMED BIRTHDAYS is a full service event planning company creating memorable BIRTHDAY PARTY DECORATIONS IN BANGALORE.
                            Our aim is to go on the journey with you from start to finish, to curate aesthetically beautiful birthdays. We have a decent collection of unique, trending and BEST BIRTHDAY THEME DECORATIONS.
                           With a pool of talented BALLOON ARTISTS IN BANGALORE we are committed to provide you the BEST BALLOON DECORATIONS IN BANGALORE.
                          </p>



                   <p style="font-size:14pt;">
                           With a vast experience,<strong> THEMED BIRTHDAYS</strong> are known to be a safe pair of hands. We provide BIRTHDAY DECORATION and BALLOON DECORATION IN BANGALORE and city outskirts. Our parties are uniquely customized and tailored to each of our clients needs making it a special experience for you and your guests as well as allowing the host to be a guest at their party. We offer creative designs for BIRTHDAY PARTIES AT HOME, APARTMENT CLUBHOUSES, BANQUET HALLS AND OUTDOOR VENUES.


                          </p>
                          <p style="font-size:16pt;">Let’s create magic, events that provokes emotions and the warmest of memories for years to come.</p>
              </div>
                  </div></div></div>
</section>
        <!-- Page Content -->
        <section id="section2">
<div class="section-5-container section-container" id="section-5">
              <div class="container">
                  <div class="row">

                      <div class="col section-5 section-description wow fadeIn">
                          <center> <u style="color:red;" >
                          <h3  style="color:red;margin-bottom: 3%;font-size: 18pt;font-weight: 800;text-transform: uppercase;">
                          Themed Birthday Party Decorators </h3></u></center>

                  </div>
                  <div class="row" style="padding:15px;margin-bottom: 3%;">
                  @foreach($decorations as $decoration)
                      <div class="col-md-4 section-5-box wow fadeInUp">
                        <div class="section-5-box-image">
                        <center><h3 style="margin-bottom: 13%;height: 52px;width: 350px;"><a href="#" style="color:red; margin-bottom: 8%;">{{$decoration['title']}}</a></h3></center>
                        <img src="/decorationsimage/{{$decoration['image']}}" alt="portfolio-1"width="100%" height="100%"></div>
                        <center>
                            <div class="product-price">
                     <span class="red">₹{{$decoration['off_price']}}.00</span>
                     <span class="old">₹{{$decoration['ac_price']}}.00</span>
                     </div>

                        <a href="/{{request()->segment(count(request()->segments()))}}/{{$decoration['id_as_title']}}" class="btn bg-red">Enquiry</a></center>
                    </div>
                    @endforeach

                  </div>


              </div>
            </div>

<!-- <center><a href="/decorations-{{request()->segment(count(request()->segments()))}}" class="btn bg-red" style="margin-bottom: 3%;">See More</a></center> -->
</section>
        <!--activities Start-->

            <!--activities close-->

<section class="content-section" data-background="#009a4e" style="background: Black;border-top: 3px solid red;box-shadow: 0 .7rem 5rem rgba(0,0,0,.15)!important;">
      <div class="col-12 spacing-100"></div>
      <div class="container">
      <h3 style="color:#fff;">  Surprises in Bangalore</h3>
            <div class="row">


              <div class="col-sm-3 section-6-box wow fadeInUp">
                    <ul class="part1" style="color:#fff;text-align: left;">
                       <li><a href="#"  style="color:#fff;">Simple birthday decoration at home</a></li>
                       <li><a href="#"  style="color:#fff;">Room Decoration services for Birthday</a></li>
                       <li><a href="#"  style="color:#fff;">Balloon arch Decoration at home</a></li>
                       <li><a href="#"  style="color:#fff;">Balloon decoration arch online</a></li>
                       <li><a href="#"  style="color:#fff;">Balloon decoration for birthday</a></li>
                       <li><a href="#"  style="color:#fff;">Anniversary decoration for Husband at home</a></li>
                    </ul>
              </div>
              <div class="col-sm-3 section-6-box wow fadeInUp">
                    <ul class="part2" style="color:#fff;text-align: left;">


                       <li><a href="#"  style="color:#fff;">Birthday decoration at home</a></li>
                       <li><a href="#"  style="color:#fff;">Home decoration with balloons</a></li>
                       <li><a href="#"  style="color:#fff;">Balloons decoration at home</a></li>
                       <li><a href="#"  style="color:#fff;">Simple birthday decoration at home with balloons</a></li>
                       <li><a href="#"  style="color:#fff;">Room decoration for Anniversary Surprise for Husband</a></li>
                    </ul>
              </div>                            <!-- end card -->
              <div class="col-sm-3 section-6-box wow fadeInUp">
                    <ul class="part3" style="color:#fff;text-align: left;">
                       <li><a href="#"  style="color:#fff;">Balloon decoration for home</a></li>
                       <li><a href="#"  style="color:#fff;">Birthday decoration in home</a></li>
                       <li><a href="#"  style="color:#fff;"> Birthday Decorations in Bangalore</a></li>
                       <li><a href="#"  style="color:#fff;">Balloon decoration in bangalore</a></li>
                       <li><a href="#"  style="color:#fff;">Balloon Decoration for Birthday at home</a></li>
                       <li><a href="#"  style="color:#fff;">Simple anniversary decoration at Home for Parents</a></li>
                    </ul>
              </div>
              <div class="col-sm-3 section-6-box wow fadeInUp">
                    <ul class="part4" style="color:#fff;text-align: left;">
                       <li><a href="#"  style="color:#fff;">Decoration for birthday At home</a></li>
                       <li><a href="#"  style="color:#fff;">Birthday decoration at home</a></li>
                       <li><a href="#"  style="color:#fff;">Room decoration for birthday</a></li>
                       <li><a href="#"  style="color:#fff;">Birthday decoration for kids</a></li>
                       <li><a href="#"  style="color:#fff;">Birthday room decoration</a></li>
                       <li><a href="#"  style="color:#fff;">Balloon Decoration for Anniversary at Home</a></li>

                    </ul>
              </div>
           </div>
        </div>

       </div>
       <div class="container">

            <div class="row">


              <div class="col-sm-3 section-6-box wow fadeInUp">
                    <ul class="part5" style="color:#fff;text-align: left;">
                       <li><a href="#"  style="color:#fff;">Kids birthday decoration at home</a></li>
                       <li><a href="#"  style="color:#fff;">Birthday decoration for kids at home </a></li>
                       <li><a href="#"  style="color:#fff;">Room decoration for Birthday Girl</a></li>
                       <li><a href="#"  style="color:#fff;">Room decoration for Birthday Boy</a></li>
                       <li><a href="#"  style="color:#fff;">Surprise Birthday decoration for Husband</a></li>
                       <li><a href="#"  style="color:#fff;">Anniversary decoration in lockdown</a></li>


                    </ul>
              </div>
              <div class="col-sm-3 section-6-box wow fadeInUp">
                    <ul class="part6" style="color:#fff;text-align: left;">

                       <li><a href="#"  style="color:#fff;">Helium Balloons Decoration</a></li>
                       <li><a href="#"  style="color:#fff;">Helium gas for balloons</a></li>
                       <li><a href="#"  style="color:#fff;">Helium balloons online delivery</a></li>
                       <li><a href="#"  style="color:#fff;">Helium balloons on ceiling</a></li>
                       <li><a href="#"  style="color:#fff;">Romantic Birthday decoration for husband</a></li>
                       <li><a href="#"  style="color:#fff;">Living Room Decoration for anniversary</a></li>
                    </ul>
              </div>                            <!-- end card -->
              <div class="col-sm-3 section-6-box wow fadeInUp">
                    <ul class="part7" style="color:#fff;text-align: left;">
                       <li><a href="#"  style="color:#fff;">Simple Room decoration for husband Birthday</a></li>
                       <li><a href="#"  style="color:#fff;">Simple Birthday decoration for Husband</a></li>
                       <li><a href="#"  style="color:#fff;">Room decoration for Birthday Surprise</a></li>
                       <li><a href="#"  style="color:#fff;">Romantic Birthday Decoration for wife</a></li>
                       <li><a href="#"  style="color:#fff;">Birthday Decoration for Wife at Home</a></li>
                       <li><a href="#"  style="color:#fff;">Birthday Decoration for Wife at Home</a></li>

                    </ul>
              </div>
              <div class="col-sm-3 section-6-box wow fadeInUp">
                    <ul class="part8" style="color:#fff;text-align: left;">
                       <li><a href="#"  style="color:#fff;">Wedding anniversary decoration</a></li>
                       <li><a href="#"  style="color:#fff;">Simple Wedding anniversary decorations at home</a></li>
                       <li><a href="#"  style="color:#fff;">1st Wedding Anniversary decorations</a></li>
                       <li><a href="#"  style="color:#fff;">25th Wedding Anniversary Decorations at Home</a></li>
                        <li><a href="#"  style="color:#fff;">Simple Room Decoration for Anniversary</a></li>

                    </ul>
              </div>
           </div>
        </div>


</section>




<div id="WAButton"></div>


<!--testimonial close-->
<section id="section3">
       <!--Footer Start-->
        <!-- Section 6 -->
        <section class="content-section" data-background="#009a4e" style="background: #fff;border-top: 3px solid red;box-shadow: 0 .7rem 5rem rgba(0,0,0,.15)!important;
}">
            <div class="col-12 spacing-100"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="accordion-box">
                            <h1 style="color:red;">FAQ'S</h1>

                            <div class="accordion" id="accordion" role="tablist">
                                <div class="row">

<div class="col-sm-5 section-6-box wow fadeInUp">
                                        <!-- end card -->
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo"> <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> What is Themed Birthday..?
                                                </a> </div>
                                            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body"> THEMED BIRTHDAYS are known to be a safe pair of hands. We provide BIRTHDAY DECORATION and BALLOON DECORATION IN BANGALORE and city outskirts.</div>
                                                <!-- end card-body -->
                                            </div>
                                            <!-- end collapse -->
                                        </div>

                                        <!-- end card -->
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo"> <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseTwo"> What is Themed Birthday..?
                                                </a> </div>
                                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body"> korfkdfkdk</div>
                                                <!-- end card-body -->
                                            </div>
                                            <!-- end collapse -->
                                        </div>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo"> <a class="collapsed" data-toggle="collapse" href="#collapse4" aria-expanded="false" aria-controls="collapseTwo"> What is Themed Birthday..?
                                                </a> </div>
                                            <div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body"> THEMED BIRTHDAYS are known to be a safe pair of hands. We provide BIRTHDAY DECORATION and BALLOON DECORATION IN BANGALORE and city outskirts.</div>
                                                <!-- end card-body -->
                                            </div>
                                            <!-- end collapse -->
                                        </div>
                                         <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo"> <a class="collapsed" data-toggle="collapse" href="#collapse5" aria-expanded="false" aria-controls="collapseTwo"> What is Themed Birthday..?
                                                </a> </div>
                                            <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body"> THEMED BIRTHDAYS are known to be a safe pair of hands. We provide BIRTHDAY DECORATION and BALLOON DECORATION IN BANGALORE and city outskirts.</div>
                                                <!-- end card-body -->
                                            </div>
                                            <!-- end collapse -->
                                        </div>


  </div>
    <div class="col-sm-4 section-6-box wow fadeInUp">
        <div class="footer-first"><senter>
       <img src="images/joy balloon.png"/ width="40%" >
        <div class="section-6-social">
			                    	<a href="#"><i class="fab fa-facebook-f"></i></a>

									<a href="#"><i class="fab fa-twitter"></i></a>
									<a href="#"><i class="fab fa-instagram"></i></a>

            </div>
         <a href="#" style="color:red;font-weight: 700;font-size: 14pt;">Site Map</a></center>
         <!--Div where the WhatsApp will be rendered-->


        </div>
     </div>
     <div class="col-sm-3 section-6-box wow fadeInUp">
    <div class="footer-last">
        <p style="font-weight: 700;font-size: 14px;color: red;"> <span class="material-icons" >call</span> +918105363088</p>
     <p style="font-weight: 700;font-size: 14px;color: red;"> <span class="material-icons">email</span> partyandevents@gmail.com </p>
         <p style="font-weight: 700;font-size: 14px;color: red;">

              Electronic City,<br>
             Bengaluru,<br>
             Karnataka, 560100
         </p>
     </div>
               </div>

                                </div>

                            </div>
                        </div>


                    </div></div>
            </div></section>



</section>






            <!-- Footer -->

            <div class="copy text-center" style="color: white">
                 <div class="container">
                <div class="col-sm-8 section-6-box wow fadeInUp" style="background: red;">
                    <p>Copyright © Themed Birthday 2021 all rights reserved</p>
                </div>
                <div class="col-sm-4 section-6-box wow fadeInUp" style="background: red;">
                    <p>Designed by<a href="www.brandingsparrows.com"> Branding Sparrow</a></p>
                </div>
            </div></div>

    <!--Footer End-->
        <!--Footer End-->
<script type="text/javascript">
   $(function () {
           $('#WAButton').floatingWhatsApp({
               phone: 'WHATSAPP-PHONE-NUMBER', //WhatsApp Business phone number
               headerTitle: 'Chat with us on WhatsApp!', //Popup Title
               popupMessage: 'Hello, how can we help you?', //Popup Message
               showPopup: true, //Enables popup display
               buttonImage: '<img src="whatsapp.svg" />', //Button Image
               //headerColor: 'crimson', //Custom header color
               //backgroundColor: 'crimson', //Custom background button color
               position: "right" //Position: left | right

           });
       });
</script>



        <!-- Optional JavaScript -->
        <script src="js/jquery-3.3.1.slim.min.js" type="text/javascript"></script>
        <script src="js/popper.min.js" type="text/javascript"></script>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>


            <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/jquery.backstretch.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/scripts.js"></script>
<script src="js/pushy.min.js"></script>



    </body>
</html>
