
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="The Best Place To Book Themed Birthday Decorations, First Birthday Party Decorations, Balloon Decoration For Birthdays At Home, Entertainment For Kids, Photographer And Videographer In Bangalore">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap4.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
         <link rel="stylesheet" href="css/style3.css" type="text/css">
          <link rel="stylesheet" href="css/animate.css">
           <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css" type="text/css">
            <link rel="stylesheet" href="css/media-queries.css" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="icon" href="favicon/favicon-16x16.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
            <link rel="stylesheet" href="css/city.css">


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


        <title>Themed Birthday Decorations At Home & Balloon Decoration For Birthdays In Bangalore | Themedbirthdays.in</title>
    </head>
    <body class="home-bg-color modal-open" style="padding-right: 17px;">







<section class="modal fade city-modal in" style="margin: auto; display: block;" id="cityChangeModal" tabindex="-1" role="dialog" aria-labelledby="cityChangeModalLabel" aria-hidden="true">
<div class="modal-dialog" style="max-width:1024px; width: 80%; "><div class="modal-content">
<div class="modal-body no-pad-t">
<div class="Title text-center">
<h5 style="font-size:19pt;font-weight:700;color:#fff;"> Popular Cities</h5>
</div>
     <center>
                    <select name="cities" id="cities" class='form-control' style="width: 50%;" >
                       <option value="" ><b>Choose Your Nearest Location</b></option>
                       <center>
                       @foreach($cities as $city)
                       <option style="font-size: 20px;" value="{{str_replace(' ','-',strtolower(($city['name'])))}}" >{{$city['name']}}</option>
                    @endforeach
                    </center>
                   </select>

     </center>





</div></div></div></div></div>
</section>



            <!-- Footer -->



    <!--Footer End-->
        <!--Footer End-->



        <!-- Optional JavaScript -->
        <script src="js/jquery-3.3.1.slim.min.js" type="text/javascript"></script>
        <script src="js/popper.min.js" type="text/javascript"></script>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>


            <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/jquery.backstretch.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/scripts.js"></script>

<script src="https://code.jquery.com/jquery-1.12.4.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(window).load(function() { // makes sure the whole site is loaded
$('#status').fadeOut(); // will first fade out the loading animation
$('#preloader').delay(50).fadeOut(100); // will fade out the white DIV that covers the website.
$('body').delay(50).css({'overflow':'visible'});
})
</script>

<script>
var url = "";
var value = "";
$(document).ready(function(){
    $("#cities").change(function(){
        city = $(this).val();
        value = city+'-bangalore'
        window.location.href = url+'balloon-decoration-near-'+value;

    });
});
</script>


    </body>
</html>
