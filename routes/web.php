<?php

use App\Models\City;
use App\Models\Decoration;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $cities = City::get();
    return view('pages.welcome',compact('cities'));
});
Route::get('/balloon-decoration-near-{city}', function () {
    $decorations = Decoration::get();
    return view('pages.home',compact('decorations'));
});
Route::get('/decorations-{city}', function () {
    $decorations = Decoration::get();
    return view('pages.see_all',compact('decorations'));
});
Route::get('/admin', function () {
    return view('admin.login');
});

Route::post('/login','App\Http\Controllers\AdminController@store');

//City
Route::get('/add_city','App\Http\Controllers\CityController@index');
Route::post('/store_city','App\Http\Controllers\CityController@store');
Route::get('/view_cities','App\Http\Controllers\CityController@show');
Route::get('/edit_city/{id}','App\Http\Controllers\CityController@edit');
Route::get('/delete_city/{id}','App\Http\Controllers\CityController@destroy');
Route::post('/update_city/{id}','App\Http\Controllers\CityController@update');
Route::get('/city_status_change/{id}','App\Http\Controllers\CityController@status_change');

//Decorations
Route::get('/decoration','App\Http\Controllers\DecorationController@index');
Route::post('/add_decoration','App\Http\Controllers\DecorationController@store');
Route::get('/view_decorations','App\Http\Controllers\DecorationController@show');
Route::get('/edit_decoration/{id}','App\Http\Controllers\DecorationController@edit');
Route::get('/delete_decoration/{id}','App\Http\Controllers\DecorationController@destroy');
Route::post('/update_decoration/{id}','App\Http\Controllers\DecorationController@update');
Route::get('/dec_status_change/{id}','App\Http\Controllers\DecorationController@status_change');
Route::get('/balloon-decoration-{city}/{title}','App\Http\Controllers\DecorationController@view_decoration');
